package helpers;

import org.openqa.selenium.WebDriver;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class TestHelper {
    public void sleepSeconds(int seconds){
        try {
            Thread.sleep(timeInSeconds(seconds));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void implicitWaitSeconds(WebDriver driver, int seconds){
        driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
    }

    //    Ejemplo de metodos utiles extra...
    public static String cleanString(final String s) {
        return s.replace("–", "-")
                .replace("\n", " ")
                .replace(" ", " ")
                .trim();
    }

    public static String parseAndFormatDate(String timestamp) {
        return parseAndFormatDate(timestamp, "MMMM d, yyyy h:mm a");
    }

    //    Metodos de uso interno
    private int timeInSeconds(int miSeconds){
        return miSeconds *1000;
    }

    public static String parseAndFormatDate(String timestamp, String expectedFormat) {
        final DateFormat finalFormat = new SimpleDateFormat(expectedFormat);
        final DateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        finalFormat.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        String formattedDate = null;
        try {
            formattedDate = finalFormat.format(timestampFormat.parse(timestamp));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedDate;
    }
}
