package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

public class SearchResultsPage extends PageObject {
    private final static String pageTitle = "Guatecompras - Busqueda por texto en concursos";
    private final static String pageUrl = "concursos/busquedaTexto.aspx?t={1}";
    private WebDriver driver;

    // elementos web de la pagina
    private By busquedaTextInput;
    private By mensajeConcursosEncontrados;

    public SearchResultsPage(WebDriver driver) {
        super(pageTitle, pageUrl);
        this.driver=driver;

        busquedaTextInput = By.id("MasterGC_ContentBlockHolder_txtTexto");
        mensajeConcursosEncontrados = By.xpath("//*[@id='MasterGC_ContentBlockHolder_lblFilas']");
    }

    public void assertCantidadResultados(String buscarTexto, Integer minResultados){
       // manipular el mansaje para obtener el entero con el resultado de ocurrencias en la busqueda.
        String mensajeResultado = driver.findElement(mensajeConcursosEncontrados).getText();
        String[] elements = mensajeResultado.split(" ");
        Integer cantidadResultados = Integer.parseInt(elements[4]);

        // obtener string buscado desde el input
        // INTENTO CON JAVASCRIPT
        //        String jsCommand = "$('#MasterGC_ContentBlockHolder_txtTexto').val()";
        //        String originalString = ((JavascriptExecutor) driver).executeScript(jsCommand).toString();

        SoftAssert softAssert = new SoftAssert();
        // validar que el string es el enviado a la busqueda
        softAssert.assertEquals(buscarTexto.toUpperCase(), driver.findElement(busquedaTextInput).getAttribute("value"));
        // validar que almenos exista la cantidad minima a evaluar
        softAssert.assertTrue(cantidadResultados > minResultados);
        softAssert.assertAll();
    }


}
