package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class LoginPage extends PageObject{

    private final static String pageTitle = "Guatecompras - Autenticarse al sistema";
    private final static String pageUrl="/priv/sistema/login.aspx";
    private WebDriver driver;

    private By userInput;
    private By userIcon;
    private By passInput;
    private By passIcon;

    public LoginPage(WebDriver driver){
        super(pageTitle, pageUrl);
        this.driver = driver;
        userInput = By.id("MasterGC_ContentBlockHolder_myLogin_txtUser");
        userIcon = By.xpath("/html/body/form/div[3]/table[2]/tbody/tr/td/div/div/div[1]/div[1]/div[1]");
        passInput = By.id("MasterGC_ContentBlockHolder_myLogin_txtPassword");
        passIcon = By.xpath("/html/body/form/div[3]/table[2]/tbody/tr/td/div/div/div[1]/div[1]/div[2]");
    }

    public void focusUserInput(){
        driver.findElement(userInput).click();
    };

    public void assertUserIcon(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(driver.findElement(userIcon).getAttribute("class"),"user-icon iconoVisible", "el icono del user NO es visible");
        softAssert.assertAll();
        System.out.println("assertUserIcon");
    }

    public void focusPassInput(){
        driver.findElement(passInput).click();
    };

    public void assertPassIcon(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(driver.findElement(passIcon).getAttribute("class"),"pass-icon iconoVisible", "el icono del user NO es visible");
        softAssert.assertAll();
        System.out.println("assertPassIcon");
    }

}
