package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends PageObject {

    private final static String pageTitle = "Guatecompras - Sistema de Contrataciones y Adquisiciones del Estado de Guatemala";
    private final static String pageUrl = "/";
    private WebDriver driver;

    private By modal;
    private By modalCloseButton;
    private By modalLogoGC;
    private By busquedaTextIcon;
    private By busquedaTextLabel;
    private By busquedaTextInput;
    private By busquedaTextSearchButton;
	private By slideShow;
	private By linkAutenticarse;


    public HomePage(WebDriver driver) {
        super(pageTitle, pageUrl);
        this.driver=driver;
        modal = By.xpath("/html/body/div[4]");
        modalCloseButton = By.xpath("/html/body/div[4]/div[1]/button");
        modalLogoGC = By.xpath("//*[@id='popUpInfoRGAE']/img");
        busquedaTextIcon = By.id("busqueda_texto_imagen");
        busquedaTextIcon = By.id("busqueda_texto_imagen");
        busquedaTextLabel = By.id("busqueda_texto_imagen_texto");
        busquedaTextInput = By.id("MasterGC_ContentBlockHolder_txtTexto");
        busquedaTextSearchButton = By.id("lblConsultaTexto");
        slideShow = By.xpath("//*[@id='MasterGC_ContentBlockHolder_slideShow']");
        linkAutenticarse = By.xpath("//*[@id='nav_login']/li/a");

    }

    public void closeModal() {
        driver.findElement(modalCloseButton).click();
    }

    public void goToLoginPage(){
        driver.findElement(linkAutenticarse).click();
    }

    public void inputTextoABuscar(String buscarTexto){
        driver.findElement(busquedaTextInput).sendKeys(buscarTexto);
    }

    public void clickBuscarPorTexto(){
        driver.findElement(busquedaTextSearchButton).click();
    }

}
