package pages;

import org.openqa.selenium.WebDriver;

public abstract class PageObject {

    protected WebDriver driver;
    private String pageTitle;
    private String pageUrl;




    public  PageObject(String pageTitle, String pageUrl) {
        this.pageTitle= pageTitle;
        this.pageUrl = pageUrl;
    }

    // code generated
    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }



}