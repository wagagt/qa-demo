package com.demo.tests;

import dataProviders.SearchDataProvider;
import helpers.TestHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.SearchResultsPage;

public class SearchTests {

    WebDriver driver;
    final String baseUrl = "http://www.guatecompras.gt";
    public HomePage homePage;
    public SearchResultsPage searchResultPage;
    private TestHelper testHelper = new TestHelper();

    @BeforeMethod
    public void beforeMethod(ITestResult result) {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("mac")) {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver");
        } else {
            System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");
        }
        driver = new ChromeDriver();
        driver.get("https://www.guatecompras.gt");

        testHelper.sleepSeconds(2);

        homePage = new HomePage(driver);
        searchResultPage = new SearchResultsPage(driver);
    }

    @Test(dataProvider = "BusquedasyResultados", dataProviderClass = SearchDataProvider.class, description = "Verifa cantidad de resultados", enabled = true, groups = {"busquedas"})
    public void testBusqueda(String buscarTexto, Integer minResultados) {
//        String buscarTexto = "escritorio";
//        Integer minResultados = 5000;
        homePage.closeModal();
        homePage.inputTextoABuscar(buscarTexto);
        homePage.clickBuscarPorTexto();
        testHelper.sleepSeconds(4); // mas adelante cambiar por WebWaitUntil (element is present )
        searchResultPage.assertCantidadResultados(buscarTexto, minResultados);
    }

    @AfterMethod
    public void afterMethod() {
        driver.quit();
        System.out.println("----------- fin del test ");
    }
}
