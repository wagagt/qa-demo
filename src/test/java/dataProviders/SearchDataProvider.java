package dataProviders;

import org.testng.annotations.DataProvider;

public class SearchDataProvider {
    @DataProvider(name = "BusquedasyResultados", parallel = false)
    public Object[][] getDataFromDataProvider() {
        return new Object[][]
                {
                        {"escritorio", 5000},
                        {"mesa", 5000},
                        {"televisor", 50},
                        {"software", 50},
                };
    }

}
